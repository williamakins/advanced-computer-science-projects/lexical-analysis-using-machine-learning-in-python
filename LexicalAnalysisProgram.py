from __future__ import division, print_function, unicode_literals

import numpy as np
import nltk
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk import FreqDist
from nltk.stem import PorterStemmer
nltk.download('wordnet')
nltk.download("stopwords")
nltk.download('averaged_perceptron_tagger')

import sklearn
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import operator
import requests

from collections import Counter

import matplotlib as mpl
import matplotlib.pyplot as plt

import string

lemmatizer = nltk.stem.WordNetLemmatizer()
stopWords = set(stopwords.words('english'))
ps = PorterStemmer()
vectorizer = CountVectorizer()

POS_WORDS = ['good', 'great', 'excellent', 'best', 'enjoy', 'enjoyable', 'fun', 'finest', 'funny', 'outstanding',
			 'nice', 'fine', 'exceptional', 'admirable', 'exemplary', 'love', 'like', 'worthwhile', 'Spectacular',
			 'genius', 'accomplishment', 'acclaimed', 'exciting', 'elegant', 'fantastic', 'fabulous', 'impressive',
			 'intelligent', 'innovative', 'phenomenal', 'pleasurable', 'pleasant', 'perfect', 'quality', 'superb',
			 'super', 'terrific', 'funniest']

NEG_WORDS = ['awful', 'abysmal', 'atrocious', 'appalling', 'bad', 'boring', 'dismal', 'dreadful', 'disgusting', 'fail',
			 'horrendous', 'hate', 'hideous', 'horrible', 'lousy', 'negative', 'nasty', 'repulsive', 'sickening',
			 'terrible', 'unpleasant', 'upset', 'vile', 'worthless']

def LoadFile(path):
	return open(path, encoding = "utf8").readlines()

def RemoveStopWords(wordString):
	wordString.lower()
	wordString = wordString.translate(str.maketrans('', '', string.punctuation)) #remove all punctuation from the review
	WordList = wordString.split(" ")

	filteredString = ""
	for word in WordList:
		if word not in stopWords:
			filteredString = filteredString + " " +  word
	return filteredString

def stemWord(word):
	return ps.stem(word)

def CalcWordFreq(ReviewList):
	words = []
	for review in ReviewList:
		review = RemoveStopWords(review)

		wordSplit = word_tokenize(review)
		wordSplit = nltk.pos_tag(wordSplit)
		for word in wordSplit:
			word = LemmatizeWord(word)
			word = stemWord(word)

			words.append(word)


	vectorizer.fit_transform(words)
	vector = vectorizer.transform(words)

	return words

def AccessFreqList(wordFreqList, freqNum):
	freq = FreqDist(wordFreqList)
	return freq.most_common(freqNum)[0][0]

def CreateVocabulary(wordFreqList, freqNum):
	freq = FreqDist(wordFreqList)

	mostCommon = freq.most_common(freqNum)
	print ("Most common words: " + str(mostCommon))

	commonWordList = []
	for word in mostCommon:
		commonWordList.append(word[0])

	print (commonWordList)

	return commonWordList

def SplitReview(review):
	review = review.lower() #convert the review to lowercase

	review_split = word_tokenize(review)
	review_split = nltk.pos_tag(review_split)

	return review_split

def LemmatizeWord(wordToken):
	#perform lemmatization on each word
	if wordToken[1].startswith('NN'):
		tag = 'n'
	elif wordToken[1].startswith('VB'):
		tag = 'v'
	else:
		tag = 'a'

	return lemmatizer.lemmatize(wordToken[0], tag)

def VectoriseReview(review):
	vectorizer = CountVectorizer()
	vectorizer.fit(review)
	return vectorizer.transform(review)

def AnalyseTrainingReviews(ReviewList, vocabulary, Y_Val, X_List, Y_List):
	reviewNum = 1

	for review in ReviewList:
		review = RemoveStopWords(review)

		reviewSplit = word_tokenize(review)
		reviewSplit = nltk.pos_tag(reviewSplit)
		
		pos_counter = 0
		neg_counter = 0

		wordVector = []

		for word in reviewSplit:
			for p_word in POS_WORDS:
				if word[0] == p_word:
					pos_counter += 1

			for p_word in NEG_WORDS:
				if word[0] == p_word:
					neg_counter -= 1

		for v in vocabulary:
			match = False;
			for word in reviewSplit:		
				word = LemmatizeWord(word)
				word = stemWord(word)

				if word == v:
					match = True
					break

			if match == True:
				wordVector.append(word)
			else:
				wordVector.append("x")

		vectorizer.fit_transform(wordVector)
		vector = vectorizer.transform(wordVector)

		outputList = vector.toarray()[0].tolist()
		print(outputList)

		outputList.append(pos_counter)
		outputList.append(neg_counter)
		X_List.append(outputList)
		Y_List.append(Y_Val)

		print(str(reviewNum) + "/" + str(len(ReviewList)) + " Reviews processed")
		reviewNum += 1


def AnalyseReviews(ReviewList, vocabulary, ResultList, GoldList, Y_Val):
	reviewNum = 1

	for review in ReviewList:
		#split up each review into single words
		review = RemoveStopWords(review)

		reviewSplit = word_tokenize(review)
		reviewSplit = nltk.pos_tag(reviewSplit)
		
		pos_counter = 0
		neg_counter = 0

		wordVector = []

		for word in reviewSplit:
			for p_word in POS_WORDS:
				if word[0] == p_word:
					pos_counter += 1

			for p_word in NEG_WORDS:
				if word[0] == p_word:
					neg_counter -= 1

		for v in vocabulary:
			match = False;
			for word in reviewSplit:		
				word = LemmatizeWord(word)
				word = stemWord(word)

				if word == v:
					match = True
					break

			if match == True:
				wordVector.append(word)
			else:
				wordVector.append("x")
				
		vectorizer.fit(wordVector)
		vector = vectorizer.transform(wordVector)

		GoldList.append(Y_Val)

		outputList = vector.toarray()[0].tolist()
		outputList.append(pos_counter)
		outputList.append(neg_counter)
		ResultList.append(outputList)

		print(str(reviewNum) + "/" + str(len(ReviewList)) + " Predicted reviews processed")
		reviewNum += 1

def main():
	pos_train_file = LoadFile('IMDb/train/imdb_train_pos.txt')
	neg_train_file = LoadFile('IMDb/train/imdb_train_neg.txt')
	#pos_train_file = LoadFile('IMDb/dev/imdb_dev_pos.txt')
	#neg_train_file = LoadFile('IMDb/dev/imdb_dev_neg.txt')

	pos_dev_file = LoadFile('IMDb/dev/imdb_dev_pos.txt')
	neg_dev_file = LoadFile('IMDb/dev/imdb_dev_neg.txt')

	pos_test_file = LoadFile('IMDb/test/imdb_test_pos.txt')
	neg_test_file = LoadFile('IMDb/test/imdb_test_neg.txt')

	X_train = []
	Y_train = []

	wordFreq = CalcWordFreq(pos_train_file)
	wordFreq += CalcWordFreq(neg_train_file)

	vocabulary = CreateVocabulary(wordFreq, 30)
	
	AnalyseTrainingReviews(pos_train_file, vocabulary, 1, X_train, Y_train)
	AnalyseTrainingReviews(neg_train_file, vocabulary, 0, X_train, Y_train)

	X_train_sentanalysis = np.asarray(X_train)
	Y_train_sentanalysis = np.asarray(Y_train)
	print(X_train_sentanalysis)
	svm_clf = sklearn.svm.SVC(kernel="linear",gamma='auto') # Load the (linear) SVM model
	svm_clf.fit(X_train_sentanalysis, Y_train_sentanalysis) # Train the SVM model

	gold = []
	test_results = []
	AnalyseReviews(pos_test_file, vocabulary, test_results, gold, 1)
	AnalyseReviews(neg_test_file, vocabulary, test_results, gold, 0)

	prediction = svm_clf.predict(test_results)

	true_negatives = 0
	true_positives = 0
	false_negatives = 0
	false_positives = 0

	for i in range(len(prediction)):
		print("Prediction: " + str(prediction[i]) + ", Gold: " + str(gold[i]))

		if prediction[i] == 0 and gold[i] == 0:
			true_negatives += 1
		elif prediction[i] == 1 and gold[i] == 1:
			true_positives += 1
		elif prediction[i] == 0 and gold[i] == 1:
			false_negatives += 1
		elif prediction[i] == 1 and gold[i] == 0:
			false_positives += 1

	accuracy = (true_positives + true_negatives) / (true_positives + true_negatives + false_positives + false_negatives)
	precision = true_positives / (true_positives + false_positives)
	recall = true_positives / (true_positives + false_negatives)
	specificity = true_negatives / (true_negatives + false_positives)
	fScore = 2 * ((precision * recall) / (precision + recall))

	print ("\nTrue Positives: " + str(true_positives))
	print ("True Negatives: " + str(true_negatives))
	print ("False Positives: " + str(false_positives))
	print ("False Negatives: " + str(false_negatives))

	print ("\nAccuracy: " + str(accuracy))
	print ("Precision: " + str(precision))
	print ("Recall / Sensitivity: " + str(recall))
	print ("Specificity: " + str(specificity))
	print ("F1-Score: " + str(fScore))

if __name__ == "__main__":
	main()